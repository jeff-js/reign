# Reign Hacker News App

## Instructions 

1. locate the file docker-compose..yml
2. create the network interface for the application "docker network create mongo-network"
3. compile the service named "mongo"
4. run the service "mongo"
5. connect to the "mongo" service as sysadmin by sh console
6. create a user with the role of readWrite
7. update the environment variables of the frontend and backend
8. services compile the backend and frontend
9. service run the backend and frontend service

To initialize the database data is to execute the following
	
	http: //{URL_BACKEND}:{PORT:BACKEND}/hacker-news/sync

After the database is populated, the process is automatically synchronized every hour with a cronjob configured from the backend.


Example configuration Docker Compose
```
version: '3.1'

services:
  backend:
    (...)
    environment:
      - MONGO_DB_NAME={COLLECTION_NAME}
      - MONGO_URI=mongodb://{SERVICE_NAME_DOCKER_MONGO}
      - MONGO_PORT={PORT_SERVICE_MONGODB}
      - MONGO_USERNAME={USERNAME_MONGODB_WITH_ROLE_READWRITE}
      - MONGO_PASSWORD ={PASSWORD_MONGODB_WITH_ROLE_READWRITE}
  frontend:
    (...)
    environment:
      - API_SSR_URL=http://{SERVICE_NAME_DOCKER_BACKEND}:3000
      - API_URL=http://{PUBLIC_HOST}:3000
  mongo:
    (...)
    environment:
      - MONGO_INITDB_DATABASE={ COLLECTION_NAME}
      - MONGO_INITDB_ROOT_USERNAME={USERNAME_SYSADMIN}
      - MONGO_INITDB_ROOT_PASSWORD={PASSWORD_SYSADMIN}
    (...)

```
## Example configurations
```
version: '3.1'

services:
  backend:
    (...)
    environment:
      - MONGO_DB_NAME=hacker_news
      - MONGO_URI=mongodb://mongo
      - MONGO_PORT =27017
      - MONGO_USERNAME=hacker_user
      - MONGO_PASSWORD=@H4K3rN3ws
  frontend:
    (...)
    environment:
      - API_SSR_URL=http://api:3000
      - API_URL=http://localhost:3000
  mongo:
    (...)
    environment:
      - MONGO_INITDB_DATABASE=hacker_news
      - MONGO_INITDB_ROOT_USERNAME=root
      - MON GO_INITDB_ROOT_PASSWORD=@H4K3rN3ws4dm1n
    (...)

```