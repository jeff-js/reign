import * as dotenv from 'dotenv';
dotenv.config();

export type AppConfig = {
  port: number;
  mongo: {
    uri: string;
    port: number;
    username: string;
    password: string;
    database: string;
  };
};

export const appConfig: AppConfig = {
  port: Number(process.env.PORT) || 3000,
  mongo: {
    database: process.env.MONGO_DB_NAME || 'reign_db',
    uri: process.env.MONGO_URI || 'mongodb://localhost',
    port: Number(process.env.MONGO_PORT) || 27017,
    username: process.env.MONGO_USERNAME || '',
    password: process.env.MONGO_PASSWORD || '',
  },
};
