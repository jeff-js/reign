import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { HackerNewsModule } from './modules/hacker-news/hacker-news.module';
import { appConfig } from './config/app.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRoot(
      `${appConfig.mongo.uri}:${appConfig.mongo.port}/${appConfig.mongo.database}`,
      {
        useNewUrlParser: true,
        user: appConfig.mongo.username,
        pass: appConfig.mongo.password,
      },
    ),
    HackerNewsModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
