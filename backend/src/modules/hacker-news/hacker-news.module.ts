import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNews, HackerNewsSchema } from './schemas/hacker-news.schema';
import {
  ExcludeHackerNews,
  ExcludeHackerNewsSchema,
} from './schemas/exclude-hacker-news.schema';
import { HttpModule } from '@nestjs/axios';
import { HackerNewsController } from './hacker-news.controller';
import { HackerNewsService } from './hacker-news.service';
import { SyncService } from './sync.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: HackerNews.name, schema: HackerNewsSchema },
      { name: ExcludeHackerNews.name, schema: ExcludeHackerNewsSchema },
    ]),
    HttpModule,
  ],
  controllers: [HackerNewsController],
  providers: [HackerNewsService, SyncService],
})
export class HackerNewsModule {}
