import { Controller, Get, Param, Post } from '@nestjs/common';
import { HackerNewsService } from './hacker-news.service';

@Controller('hacker-news')
export class HackerNewsController {
  constructor(private readonly hackerNewsService: HackerNewsService) {}

  @Get()
  async findNews() {
    return this.hackerNewsService.getHackerNews();
  }

  @Get('sync')
  async findAll() {
    await this.hackerNewsService.getHackerNewsFromExternal();
    return 'OK';
  }

  @Post(':objectId')
  async excludeDocument(@Param('objectId') objectId: string) {
    return this.hackerNewsService.excludeHackerNews(objectId);
  }
}
