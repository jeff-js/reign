import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HackerNewsDocument = HackerNews & Document;

@Schema()
export class HackerNews {
  @Prop()
  title: string;

  @Prop()
  author: string;

  @Prop()
  createdAt: Date;

  @Prop()
  url: string;

  @Prop()
  objectId: string;
}

export const HackerNewsSchema = SchemaFactory.createForClass(HackerNews);
