import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ExcludeHackerNewsDocument = ExcludeHackerNews & Document;

@Schema()
export class ExcludeHackerNews {
  @Prop()
  objectId: string;
}

export const ExcludeHackerNewsSchema =
  SchemaFactory.createForClass(ExcludeHackerNews);
