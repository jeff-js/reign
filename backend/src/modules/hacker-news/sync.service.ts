import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { HackerNewsService } from './hacker-news.service';

@Injectable()
export class SyncService {
  private readonly logger = new Logger(SyncService.name);

  constructor(private readonly hackerNewsService: HackerNewsService) {}

  @Cron(CronExpression.EVERY_HOUR)
  async syncDataCron() {
    await this.hackerNewsService.getHackerNewsFromExternal();
    this.logger.debug(
      `Called when the current second is ${CronExpression.EVERY_HOUR}`,
    );
  }
}
