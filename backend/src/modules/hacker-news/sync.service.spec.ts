import { Test, TestingModule } from '@nestjs/testing';
import { SyncService } from './sync.service';
import { HackerNewsService } from './hacker-news.service';

describe('SyncService', () => {
  let service: SyncService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SyncService,
        {
          provide: HackerNewsService,
          useFactory: () => ({}),
        },
      ],
    }).compile();

    service = module.get<SyncService>(SyncService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
