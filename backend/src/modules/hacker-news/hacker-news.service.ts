import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HackerNews, HackerNewsDocument } from './schemas/hacker-news.schema';
import { HttpService } from '@nestjs/axios';
import {
  ExcludeHackerNews,
  ExcludeHackerNewsDocument,
} from './schemas/exclude-hacker-news.schema';

@Injectable()
export class HackerNewsService {
  private readonly logger = new Logger(HackerNewsService.name);

  constructor(
    @InjectModel(HackerNews.name)
    private readonly hackerNewsModel: Model<HackerNewsDocument>,
    @InjectModel(ExcludeHackerNews.name)
    private readonly excludeHackerNewsModel: Model<ExcludeHackerNewsDocument>,
    private readonly httpService: HttpService,
  ) {}

  public async getHackerNews(): Promise<HackerNewsDocument[]> {
    return this.hackerNewsModel.aggregate([
      {
        $lookup: {
          from: 'excludehackernews',
          localField: 'objectId',
          foreignField: 'objectId',
          as: 'ex',
        },
      },
      {
        $match: {
          'ex.objectId': { $exists: false },
        },
      },
      {
        $sort: {
          createdAt: -1,
        },
      },
      {
        $project: {
          _id: 1,
          author: 1,
          createdAt: 1,
          objectId: 1,
          title: 1,
          url: 1,
        },
      },
    ]);
  }

  public async getHackerNewsFromExternal() {
    try {
      await this.hackerNewsModel.deleteMany();
      const { data } = await this.httpService
        .get(
          'https://hn.algolia.com/api/v1/search_by_date?query=nodejs&hitsPerPage=100',
        )
        .toPromise();
      const hackerNews = data.hits.map((item) => {
        if (item.story_title || item.title) {
          return {
            title: item.story_title || item.title,
            url: item.url || item.story_url,
            author: item.author,
            createdAt: item.created_at,
            objectId: item.objectID,
          };
        }
      });
      await this.hackerNewsModel.insertMany(hackerNews);
    } catch (e) {
      this.logger.error(e);
    }
  }

  public async excludeHackerNews(objectId: string) {
    try {
      const hackerNews = await this.hackerNewsModel.findOne({ objectId });
      if (hackerNews) {
        return await this.excludeHackerNewsModel.create({
          objectId: hackerNews.objectId,
        });
      } else {
        return null;
      }
    } catch (e) {
      this.logger.error(e);
      return null;
    }
  }
}
