import type { NextPage } from 'next'
import React from 'react';
import axios from 'axios';
import Header from "../components/header.component";
import NewsComponent from "../components/news.component";
import moment from 'moment';
import {NewsModel} from "../types/types";
import getConfig from 'next/config'
moment.locale('en');
const { serverRuntimeConfig } = getConfig()
class Home extends React.Component<{ news: any }> {
    render() {
        const news : NewsModel[]  = this.props.news;
        return (
            <>
                <Header/>
                <NewsComponent news={news} />
            </>
        )
    }
}

export const getServerSideProps = async () => {
    const {data: items} = await axios.get(`${serverRuntimeConfig.baseSSRUrl}/hacker-news`);
    return {
        props: {
            news: items.map((item: { createdAt: any; })=>{
                return {
                    ...item,
                    createdAt: item.createdAt
                }
            })
        }
    }
};
export default Home
