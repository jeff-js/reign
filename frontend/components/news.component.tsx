import React, {useState} from 'react';
import Image from 'next/image';
import trash from '/public/trash.svg';
import styles from '../styles/news.module.sass';
import Link from "next/link";
import axios from "axios";
import moment from "moment";
import {NewsModel} from "../types/types";
import getConfig from "next/config";
moment.locale('en');
const { publicRuntimeConfig } = getConfig()
const NewsComponent = (props: any ) => {
    const [getNews, setNews] = useState(props.news);
    const deleteItem = async ({item}: { item: any }) =>{
    await axios.post(`${publicRuntimeConfig.basePublicUrl}/hacker-news/${item.objectId}`, {})
    const {data: items} = await axios.get( `${publicRuntimeConfig.basePublicUrl}/hacker-news`);
        setNews(items)
    }

    const dateString = (date: Date) => {
        const isToday = (dateParameter: any) =>{
            const dateP = new Date(dateParameter);
            const today = new Date();
            return dateP.getDate() === today.getDate() && dateP.getMonth() === today.getMonth() && dateP.getFullYear() === today.getFullYear();
        }
        const yesterday = (dateParameter: any) =>{
            const dateP = new Date(dateParameter);
            const yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1);
            return dateP.getDate() === yesterday.getDate() && dateP.getMonth() === yesterday.getMonth() && dateP.getFullYear() === yesterday.getFullYear();
        }
        if(isToday(moment(date))){
            return moment(date).format('HH:mm A');
        } else if (yesterday(moment(date))){
            return 'Yesterday';
        } else {
            return moment(date).format('MMM DD');
        }
    }

  return (
    <div className={styles.news}>
        {getNews && getNews.map((item: NewsModel) => (
            <div key={item.objectId}>
              {item.url &&
              <div  className={styles.newsItem}>
                <Link href={item.url}>
                  <a target={'_blank'}>
                      <div className={styles.titleColumn}><span className={styles.titleRow}>{item.title} { '  ' }</span>  <span className={styles.subTitle}> { ' - ' }{item.author} -</span></div>
                  </a>
                </Link>
                <div className={styles.newsDateActions}>
                  <div>{dateString(item.createdAt)}</div>
                  <div className={styles.icon}>
                    <Image src={trash} onClick={()=>deleteItem({item: item})} alt={'trash'} width={10} height={10} />
                  </div>
                </div>
              </div>
              }
              {!item.url &&
                <div  className={styles.newsItem}>
                    <div className={styles.titleColumn}><span className={styles.titleRow}>{item.title} { '  ' }</span>  <span className={styles.subTitle}> { ' - ' }{item.author} -</span></div>
                  <div className={styles.newsDateActions}>
                    <div>{dateString(item.createdAt)}</div>
                    <div className={styles.icon}>
                      <Image onClick={()=>deleteItem({item: item})} src={trash} alt={'trash'} width={10} height={10} />
                    </div>
                  </div>
                </div>
              }
            </div>
        ))}
    </div>
  );
};

export default NewsComponent;
