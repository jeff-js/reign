import styles from '../styles/header.module.sass';
function Header() {
    return (
        <>
            <header className={styles.header}>
                <h1>HN Feed</h1>
                <h4>We ♥️ hacker news!</h4>
            </header>
        </>
    )
}

export default Header
