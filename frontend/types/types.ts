export type NewsModel = {
    _id: string;
    title: string;
    author: string;
    createdAt: Date;
    url: string;
    objectId: string;
}
