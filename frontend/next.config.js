/** @type {import('next').NextConfig} */
const path = require('path')
require('dotenv').config();
const baseSSRUrl = process.env.API_SSR_URL || 'http://localhost:3000';
const basePublicUrl = process.env.API_URL || 'http://localhost:3000';


const nextConfig = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  env:{
    baseSSRUrl: baseSSRUrl,
    basePublicUrl: basePublicUrl
  },
  publicRuntimeConfig: {
    basePublicUrl: basePublicUrl,
  },
  serverRuntimeConfig: {
    baseSSRUrl: baseSSRUrl
  },
}

module.exports = nextConfig
